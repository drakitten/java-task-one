INSERT INTO Groups (id, name) VALUES
(1, 'CS'),
(2, 'SE'),
(3, 'IT');

INSERT INTO Students (id, name, phone, groupId) VALUES
(1, 'Venera', '87761742853', 1),
(2, 'Dima', '87015882882', 3),
(3, 'Arai', '87123456789', 2),
(4, 'Bek', '87047044444', 1);

/*SELECT * FROM Students ORDER BY groupId;*/