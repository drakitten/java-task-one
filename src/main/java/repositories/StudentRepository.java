package repositories;

import models.Student;

import java.sql.*;

public class StudentRepository {
    private final String url = "jdbc:postgresql://localhost:5433/university";
    private final String user = "postgres";
    private final String password = "damirbek";

    Connection conn = null;
    Statement stmt = null;

    public void printAllStudents() {
        try {
            //connect
            conn = DriverManager.getConnection(url, user, password);
            //create statement
            stmt = conn.createStatement();
            String sql = "SELECT * FROM students ORDER BY groupId";
            ResultSet rs = stmt.executeQuery(sql);
            //output
            int i = 0;
            while(rs.next()) {
                i++;
                Student std = new Student(rs.getInt("id"), rs.getString("name"),
                        rs.getString("phone"), rs.getInt("groupId"));

                System.out.println(std.toString());
            }
            rs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch(SQLException se){}

            try {
                if (conn != null) conn.close();
            } catch(SQLException se){
                se.printStackTrace();
            }
        }
        System.out.println("That's all students!");
    }
}
