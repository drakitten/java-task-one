package repositories;

import models.Group;

import java.sql.*;

public class GroupRepository {
    private final String url = "jdbc:postgresql://localhost:5433/university";
    private final String user = "postgres";
    private final String password = "damirbek";

    Connection conn = null;
    Statement stmt = null;

    public void printAllGroups() {
        try {
            conn = DriverManager.getConnection(url, user, password);

            stmt = conn.createStatement();
            String sql = "SELECT * FROM groups";
            ResultSet rs = stmt.executeQuery(sql);

            while(rs.next()) {
                Group gr = new Group(rs.getInt(1), rs.getString(2));
                System.out.println(gr.toString());
            }
            rs.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se) {
            }

            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        System.out.println("That's all groups!");
    }
}
