import repositories.GroupRepository;
import repositories.StudentRepository;

public class Main {

    public static void main(String[] args) {

        StudentRepository sr = new StudentRepository();
        sr.printAllStudents();

        System.out.println("-----------------------------");

        GroupRepository gr = new GroupRepository();
        gr.printAllGroups();
    }
}
